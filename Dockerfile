FROM alpine:latest as packages
RUN set -euxo pipefail; \
    apk update; \
    apk add --no-cache \
        ca-certificates \
        curl \
        git \
        make \
        jq \
        wget;

FROM packages as helm-builder
RUN set -euxo pipefail; \
    mkdir --parents /tmp/helm; \
    tag=$( curl --header 'Accept: application/vnd.github.v3+json' "https://api.github.com/repos/helm/helm/releases" \
      | jq --raw-output '. | map(select(.draft == false)) | map(select(.prerelease == false)) | .[0] | .tag_name' ) \
    && curl --show-error --location "https://get.helm.sh/helm-${tag}-linux-amd64.tar.gz" | tar --extract --gzip --directory /tmp/helm; \
    mv /tmp/helm/linux-amd64/helm /usr/bin/helm; \
    rm -rf /tmp/helm

FROM packages
COPY --from=helm-builder /usr/bin/helm /usr/bin/helm
RUN set -euxo pipefail; \
    mkdir --parents /opt/helm-chart-repostiory-builder
COPY bin /opt/helm-chart-repostiory-builder/bin
COPY assets /opt/helm-chart-repostiory-builder/assets
ENV PATH="/opt/helm-chart-repostiory-builder/bin:${PATH}"
